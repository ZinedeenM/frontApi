import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PagesComponent } from './Components/pages/pages.component';
import {MatTabsModule} from '@angular/material/tabs';
import { JustifyComponnentComponent } from './Components/justify-componnent/justify-componnent.component';
import { ContainerComponent } from './Components/container/container.component';
import {ContainerDirective} from './Components/ContainerDirective';

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    JustifyComponnentComponent,
    ContainerComponent,
    ContainerDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
