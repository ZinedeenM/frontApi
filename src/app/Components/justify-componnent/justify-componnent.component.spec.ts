import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JustifyComponnentComponent } from './justify-componnent.component';

describe('JustifyComponnentComponent', () => {
  let component: JustifyComponnentComponent;
  let fixture: ComponentFixture<JustifyComponnentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JustifyComponnentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JustifyComponnentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
