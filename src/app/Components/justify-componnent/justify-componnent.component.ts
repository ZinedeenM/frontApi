import { Component, OnInit } from '@angular/core';
import {ContainerInterface} from '../ContainerInterface';

@Component({
  selector: 'app-justify-componnent',
  templateUrl: './justify-componnent.component.html',
  styleUrls: ['./justify-componnent.component.scss']
})
export class JustifyComponnentComponent implements OnInit, ContainerInterface {

  data: any;
  constructor() {
    this.data = 'justified_componnent';
  }

  ngOnInit(): void {
  }


}
