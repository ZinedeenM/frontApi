import {Component, ComponentFactoryResolver, Inject, OnInit} from '@angular/core';
import {Page} from '../../Models/Page';
import {Container} from '../../Models/Container';
import {ContainerService} from '../../Services/container.service';
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  public pages?: Page[];

  constructor(private containerService: ContainerService,
              private componentFactoryResolver: ComponentFactoryResolver) {
  }

  public getComponent(container: Container): ContainerItem{
    return  this.containerService.getComponent(container);
  }

  ngOnInit(): void {
    this.pages = [
      new Page('Test1', 'url1', new Container('cont1', 'justify_component', [])),
      new Page('Test2', 'url2', new Container('cont2', 'type', [])),
      new Page('Test3', 'url3', new Container('cont3', 'type', [])),
    ];
  }

}


import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {ContainerItem} from '../../Models/ContainerItem';
import {ContainerInterface} from '../ContainerInterface';
