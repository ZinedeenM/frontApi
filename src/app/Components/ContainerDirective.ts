// tslint:disable: directive-selector
import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[componentHost]',
})
export class ContainerDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

