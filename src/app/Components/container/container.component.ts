import {Component, ComponentFactoryResolver, Input, OnInit, ViewChild} from '@angular/core';
import {ContainerDirective} from '../ContainerDirective';
import {ContainerItem} from '../../Models/ContainerItem';
import {ContainerInterface} from '../ContainerInterface';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  // @ts-ignore
  @Input() container: ContainerItem;
  // @ts-ignore
  @ViewChild(ContainerDirective, {static: true}) componentHost: ContainerDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this.loadComponent();
  }

  loadComponent(): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.container.component);

    const viewContainerRef = this.componentHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<ContainerInterface>(componentFactory);
    componentRef.instance.data = this.container.data;
  }
}
