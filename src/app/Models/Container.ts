export class Container{
  constructor(public label: string,
              public type: string,
              public childs: Container[],
              public parent?: Container) {
  }
}
