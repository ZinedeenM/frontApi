import {Container} from './Container';

export class Page{
  constructor(public label: string,
              public url: string,
              public container: Container) {
  }
}
