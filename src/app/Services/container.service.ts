import {Injectable} from '@angular/core';
import {Container} from '../Models/Container';
import {JustifyComponnentComponent} from '../Components/justify-componnent/justify-componnent.component';
import {ContainerItem} from '../Models/ContainerItem';

@Injectable({
  providedIn: 'root'
})
export class ContainerService {

  constructor() {
  }

  getComponent(container: Container): ContainerItem {
    switch (container.type) {
      case 'justify_component':
        return new ContainerItem(JustifyComponnentComponent, {});
      default:
        return new ContainerItem(JustifyComponnentComponent, {});
    }
  }
}
